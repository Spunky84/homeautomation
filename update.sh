#!/usr/bin/env bash
# script for updating symlinks, so openhab will reload the files

openhab_dir="/etc/openhab2"
config_dir="$(realpath "$0")"
openhab_conf_dir="$(dirname "$config_dir")/openhab2"

array=( $(find $openhab_conf_dir -mindepth 2 -type f) );
for file in "${array[@]}"
do
  symlink="$openhab_dir/$(echo $file | sed 's/.*openhab2\///')"
  file_time=$(stat -c %Y $file 2> /dev/null)
  symlink_time=$(stat -c %Y $symlink 2> /dev/null)
  if ! [ -z $file_time ] && ! [ -z $symlink_time ] && [ $file_time -gt $symlink_time ]
  then
    echo "touched $symlink"
    touch -h $symlink
  fi
done
