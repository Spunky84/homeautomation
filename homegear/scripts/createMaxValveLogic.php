#!/usr/bin/env php
//Source: https://forum.homegear.eu/t/HM-CC-RT-DN-und-VALVESTATE-setzen/391/10

/*
         ID │ Name                      │  Address │ Serial Number │ Type │ Type String               │ Firmware │ Config Pending │ Unr$
────────────┼───────────────────────────┼──────────┼───────────────┼──────┼───────────────────────────┼──────────┼────────────────┼───$
            │                           │          │               │      │                           │          │                │   $
          2 │ 'WiGa FenMit'             │   5A172D │    OEQ0429022 │ 00C7 │ HM-Sec-SCo                │      1.0 │             No │   $
          3 │ 'WoZi Heiz'               │   5FA32A │    OEQ1695652 │ 0095 │ HM-CC-RT-DN               │      1.4 │             No │   $
          4 │ 'WiGa HeizLi'             │   5FA31C │    OEQ1695668 │ 0095 │ HM-CC-RT-DN               │      1.4 │             No │   $
          5 │ 'WiGa HeizRe'             │   5FA31D │    OEQ1695666 │ 0095 │ HM-CC-RT-DN               │      1.4 │             No │   $
          6 │ 'Flur EcoTaster'          │   5A020D │    OEQ0483092 │ 006B │ HM-PB-2-WM55              │      1.4 │             No │   $
          7 │ 'WoZi Therm'              │   618192 │    OEQ1665642 │ 00AD │ HM-TC-IT-WM-W-EU          │      1.3 │             No │   $
          8 │ "Bad Fen"                 │   5BD457 │    OEQ0701741 │ 00C7 │ HM-Sec-SCo                │      1.0 │             No │   $
          9 │ "Bad Therm"               │   63427E │    OEQ1675893 │ 00AD │ HM-TC-IT-WM-W-EU          │      1.3 │             No │   $
         10 │ "Bad HeizFen"             │   63A200 │    OEQ1718317 │ 0095 │ HM-CC-RT-DN               │      1.4 │             No │   $
         11 │ "Bad HeizHand"            │   62ED2A │    OEQ1708072 │ 0095 │ HM-CC-RT-DN               │      1.4 │             No │   $
────────────┴───────────────────────────┴──────────┴───────────────┴──────┴───────────────────────────┴──────────┴────────────────┴───$
*/

<?php

  $hg = new \Homegear\Homegear();

  // TODO alle hm-cc-rt-dn holen und Variablen basierend auf dem Namen erstellen
  //$devices = $hg->listDevices();


  foreach(array(3 => "HKT1",
                4 => "HKT2",
                5 => "HKT3",
                10 => "HKT4",
                11 => "HKT5") as $deviceId => $deviceName) {

    print_r($hg->deleteSystemVariable("maxValveState" . $deviceName));

    print_r($hg->setSystemVariable("maxValveState" . $deviceName, 23));

    print_r($hg->getAllSystemVariables());


    try {
      print_r($hg->removeEvent("setMaxValveState" . $deviceName));
    } catch (Exception $e) {
      print("no event to remove for room " . $deviceName);
    }

    print_r($hg->addEvent(
      array(
        "TYPE" => 0,
        "ID" => "setMaxValveState" . $deviceName,
        "VARIABLE" => "maxValveState" . $deviceName,
        "TRIGGER" => 2,

        "EVENTMETHOD" => "runScript",
        "EVENTMETHODPARAMS" => Array("setMaxValveState.php", $deviceId . " " . $deviceName),
      )
    ));

    print_r($hg->listEvents());
  }
?>
