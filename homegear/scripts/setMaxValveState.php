#!/urs/bin/env php
//Source: https://forum.homegear.eu/t/HM-CC-RT-DN-und-VALVESTATE-setzen/391/10


// 1. Argument: ID des Gerätes in homegear
// 2. Argument: Name des Gerätes

<?php
  $deviceId = inval($argv[1]);
  $deviceName = $argv[2];

  /**** Use built-in script engine ****/
  $hg = new \Homegear\Homegear();

  $percentage = $hg->getSystemVariable("maxValveState" . $deviceName);

  if($percentage >= 0 && $percentage <= 100) {
    $hg->putParamset($deviceId, -1, "MASTER", array("VALVE_MAXIMUM_POSITION" => $percentage)
    );
  }
?>
