#!/usr/bin/env bash

#written by Martin Streu
#martinstreu@gmail.com

# Make sure only root can run our script
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

# globals
openhab_dir="/etc/openhab2"
openhab_usr="openhab"
openhab_grp="openhab"
openhab_newlinks=false
homegear_dir="/var/lib/homegear"
homegear_usr="homegear"
homegear_grp="homegear"
homegear_newlinks=false

config_dir="$(realpath "$0")"
openhab_conf_dir="$(dirname "$config_dir")/openhab2"
homegear_conf_dir="$(dirname "$config_dir")/homegear"

#homegear config
array=( $(find $homegear_conf_dir -mindepth 2 -type f) );

for i in "${array[@]}"
do
  new_link="$homegear_dir/$(echo $i | sed 's/.*homegear\///')"
  #backup existing files
	if [ -e $new_link ]
	then
		if ! [ -h $new_link ]
		then
			echo "$new_link exists as file"
			new_link_backup=$new_link"_backup"
			echo "creating backup file $new_link_backup"
			rsync -a $new_link $new_link_backup
			rm $new_link
		fi
	fi
	#creating symlink
	if [ -e $new_link ]
	then
		rm $new_link
  else
    homegear_newlinks=true
	fi
	echo "creating symlink $new_link -> $i"
	ln -s $i $new_link
	chown -h $homegear_usr:$homegear_grp $new_link
done
#restart homegear
if [ homegear_newlinks == true ]
then
  echo "restarting homegear.service"
  systemctl restart homegear.service
fi


#openhab2 config
array=( $(find $openhab_conf_dir -mindepth 2 -type f) );

for i in "${array[@]}"
do
	new_link="$openhab_dir/$(echo $i | sed 's/.*openhab2\///')"
	#backup existing files
	if [ -e $new_link ]
	then
		if ! [ -h $new_link ]
		then
			echo "$new_link exists as file"
			new_link_backup=$new_link"_backup"
			echo "creating backup file $new_link_backup"
			rsync -a $new_link $new_link_backup
			rm $new_link
		fi
	fi
	#creating symlink
	if [ -e $new_link ]
	then
		rm $new_link
  else
    openhab_newlinks=true
	fi
	echo "creating symlink $new_link -> $i"
	ln -s $i $new_link
	chown -h $openhab_usr:$openhab_grp $new_link
done

# restart openhab2
if [ openhab_newlinks == true ]
then
  echo "restarting openhab2.service"
  systemctl restart openhab2.service
fi

exit
